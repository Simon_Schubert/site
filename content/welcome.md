+++
title = "Welcome to Veloren!"
description = "A welcome to the Veloren development blog"

date = 2019-05-28
weight = 0
+++

Veloren is an open-world, open-source multiplayer voxel RPG. The game is in an early stage of development, but is playable.

# Join Us

Veloren is open-source! Join us in making this game the best it can be.

* [Gitlab - Grab the source](https://gitlab.com/veloren/veloren)
* [Discord - Join the discussion](https://discord.gg/ecUxc9N)
* [Reddit - Get the latest updates to this blog at r/Veloren](https://www.reddit.com/r/Veloren/)

# Downloads

## Nightly

Veloren is currently under heavy development. The nightly builds include many new features and bug fixes for issues in the last stable release, and are currently the recommended version for playing the game.  

Please note that the public nightly builds are a new feature and may occasionally be unavailable.

## Windows x64

[Download for Windows x64](https://www.airshipper.songtronix.com/latest/windows)

To play the game, extract all files and run `veloren-voxygen.exe`.

### Linux x64

[Download for Linux x64](https://www.airshipper.songtronix.com/latest/linux)

To play the game, extract all files and run `veloren-voxygen`.

### Mac OS

While Veloren does run on Mac OS, we don't currently produce builds for the platform. You can build Veloren from source yourself using the instructions in [the book](https://book.veloren.net).

## 0.3.0

## Windows x64

[Download for Windows x64](https://gitlab.com/veloren/veloren/-/jobs/265513559/artifacts/raw/optional-release-windows-debug.zip)

To play the game, extract all files and run `veloren-voxygen.exe`.

### Linux x64

[Download for Linux x64](https://gitlab.com/veloren/veloren/-/jobs/265513558/artifacts/raw/optional-release-linux-debug.tar.bz2)

To play the game, extract all files and run `veloren-voxygen`.

### Mac OS

While Veloren does run on Mac OS, we don't currently produce builds for the platform. You can build Veloren from source yourself using the instructions in [the book](https://book.veloren.net).

## 0.2.0

## Windows x64

[Download for Windows x64](https://gitlab.com/veloren/veloren/-/jobs/220570218/artifacts/raw/commit-windows-debug.zip)

To play the game, extract all files and run `veloren-voxygen.exe`.

### Linux x64

[Download for Linux x64](https://gitlab.com/veloren/veloren/-/jobs/220570217/artifacts/raw/commit-linux-debug.tar.bz2)

To play the game, extract all files and run `veloren-voxygen`.

### Mac OS

While Veloren does run on Mac OS, we don't currently produce builds for the platform. You can build Veloren from source yourself using the instructions in [the book](https://book.veloren.net).

## 0.1.1

**NOTICE: The public server for this release has moved to `veloren.mac94.de:38888`. This must be entered as a custom address when running the game.**

[Download for Windows x64](https://gitlab.com/veloren/game/-/jobs/artifacts/master/download?job=nightly-windows-debug)

To play the game, extract all files and run `voxygen.exe`, the 3D frontend.

If you want to host your own local server, run `server-cli.exe` in the background.

[Download for Linux x64](https://gitlab.com/veloren/game/-/jobs/artifacts/master/download?job=nightly-linux-debug)

To play the game, extract all files and run `voxygen`, the 3D frontend.

*Please Note: Voxygen currently has a command-line startup interface, so must be run from a terminal.*

If you want to host your own local server, run `server-cli` in the background.

## 0.1.0

**NOTICE: The public server for this release has been shut down. You can still play by running a local server, however.**

### Windows x64

[Download for Windows x64](https://gitlab.com/veloren/game/-/jobs/artifacts/v0.1.0/download?job=stable-windows-optimized)

To play the game, extract all files and run `voxygen.exe`, the 3D frontend.

If you want to host your own local server, run `server-cli.exe` in the background.

### Linux x64

[Download for Linux x64](https://gitlab.com/veloren/game/-/jobs/artifacts/v0.1.0/download?job=stable-linux-optimized)

To play the game, extract all files and run `voxygen`, the 3D frontend.

*Please Note: Voxygen currently has a command-line startup interface, so must be run from a terminal.*

If you want to host your own local server, run `server-cli` in the background.

### macOS

[Download for macOS](/download/macos.zip)

To run the game, extract all files and run `./voxygen`, the 3D frontend, from a terminal window.
Unfortunately due to a keyboard key mapping issue you cannot move around in this version.

*We do not currently provide a server build for macOS. If you wish to run a Veloren server on macOS, you can [compile it yourself](https://gitlab.com/veloren/game/wikis/Developer's-Corner/Introduction).*
